import json
import requests
from .models import ConferenceVO

# import time


def get_conferences():
    url = "http://monolith:8000/api/conferences/"  # monolith-1?
    print("pulling")
    response = requests.get(url)
    print("pulled")
    content = json.loads(response.content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )


# def poll():
#     while True:
#         try:
#             get_conferences()
#         except Exception as e:
#             print(e)
#         time.sleep(60)
