import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    json_picture = requests.get(
        url="https://api.pexels.com/v1/search",
        params={"query": (city, state)},
        headers={"Authorization": PEXELS_API_KEY},
    )
    picture = json.loads(json_picture.content)
    if len(picture["photos"]) == 0:
        picture_url = {"picture_url": None}
        return picture_url
    picture_url = {"picture_url": picture["photos"][0]["src"]["original"]}
    return picture_url


get_photo("hello", "potatohead")


def get_weather_data(city, state):
    json_coordinates = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    )
    python_coordinates = json.loads(json_coordinates.content)
    lat = python_coordinates[0]["lat"]
    lon = python_coordinates[0]["lon"]
    json_weather = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    )
    python_weather = json.loads(json_weather.content)
    weather = {
        "weather": {
            "temp": python_weather["main"]["temp"],
            "description": python_weather["weather"][0]["main"],
        }
    }
    return weather


""" 
    Jake's Method:
    
    def get_lat_lon(city, state):
    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = { "q": f"{location.city}, {location.state.abbreviation}"},USA",
    "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    return {"latitude": parsed_json[0]["lat"], "longitude": parsed_json[0]["lon"],}


def get_weather_data(location):
    lat_long = get_lat_long(location)
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        'lat': lat_long["latitude"],
        'lon': lat_long["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial" 
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    weather_data = {"temp": parsed_json["main"]["temp"], "description": parsed_json["weather"][0]["description"]}
    return weather_data
 """
